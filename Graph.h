#define _CRT_SECURE_NO_WARNINGS
#include <algorithm>
#include <string>
#include <iostream>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <set>
#include <tuple>
#include <map>
#include <stack>
#include <queue>

using namespace std;

bool cmpKruskal(const tuple <int, int, int> &t1, const tuple <int, int, int> &t2)
{
	if (get<2>(t1) != get<2>(t2)) return (get<2>(t1) < get<2>(t2));
	else if (get<0>(t1) != get<0>(t2)) return (get<0>(t1) < get<0>(t2));
	else return (get<1>(t1) < get<1>(t2));
}

class DSU
{
private:
	vector <int> pred;
	vector <int> rank;
public:
	DSU(int n)
	{
		pred.resize(n + 1);
		rank.resize(n + 1);
		for (int i = 1; i <= n; i++)
		{
			pred[i] = i;
			rank[i] = 1;
		}
	}

	int getPred(int i)
	{
		return pred[i];
	}

	int find(int x)
	{
		if (x == pred[x]) return x;
		return pred[x] = find(pred[x]); //Path compressing
	}

	bool checkConnectivity()
	{
		for (int i = 1; i < pred.size() - 1; i++)
		{
			if (find(pred[i]) != find(pred[i + 1])) return false;
		}
		return true;
	}

	void unite(int x, int y)
	{
		x = find(x);
		y = find(y);
		if (x != y)
		{
			if (rank[x] < rank[y]) swap(x, y); //Rank matters, rank = tree size, not tree depth
			pred[y] = x;
			rank[x] += rank[y];
		}
	}
};

class Graph
{
private:
	char vState = 'E';
	int rState = 0, wState = 0, n = 0, m = 0;
	vector <map <int, int> > AdjList;
	vector <vector <int> >  AdjMatrix;
	vector <tuple <int, int, int> >  ListOfEdges;

	void setStates(char v, int r, int w, int nTmp, int mTmp)
	{
		vState = v;
		rState = r;
		wState = w;
		n = nTmp;
		m = mTmp;
		switch (vState)
		{
		case 'L':
		{
			AdjList.resize(n);
		}
			break;
		case 'C':
		{
			AdjMatrix.resize(n);
			for (int i = 0; i < n; i++)
				AdjMatrix[i].resize(n);
		}
			break;
		case 'E':
		{
			ListOfEdges.resize(m);
		}
			break;
		}
	}

public:
	Graph(){}
	
	void readGraph(string fileName)
	{
		AdjList.clear();
		AdjMatrix.clear();
		ListOfEdges.clear();
		const char * cName = fileName.c_str();
		freopen(cName, "r", stdin);
		cin >> vState;
		switch (vState)
		{
		case 'L':
		{
			string str = "";
			cin >> n >> rState >> wState;
			AdjList.resize(n);
			getline(cin, str);
			for (int i = 0; i < n; i++)
			{
				getline(cin, str);
				istringstream a(str);
				int k, w = 1;
				while (a && str.size() != 0)
				{
					a >> k;
					if (wState)
					{
						a >> w;
					}
					AdjList[i][k] = w;
					if (!rState) AdjList[k - 1][i + 1] = w; 
				}
				m += AdjList[i].size();
			}
		}
			break;
		case 'C':
			cin >> n >> rState >> wState;
			AdjMatrix.resize(n);
			for (int i = 0; i < n; i++)
				AdjMatrix[i].resize(n);
			for (int i = 0; i < n; i++)
			{
				for (int j = 0; j < n; j++)
				{
					int w;
					cin >> w;
					if (w) m++;
					AdjMatrix[i][j] = w;
					if (!rState) AdjMatrix[j][i] = w;
				}
			}
			if (!rState) m /= 2;
			break;
		case 'E':
			cin >> n >> m >> rState >> wState;
			ListOfEdges.resize(m);
			for (int i = 0; i < m; i++)
			{
				int ai, bi, w = 1;
				if (wState)
					cin >> ai >> bi >> w;
				else
					cin >> ai >> bi;
				get<0>(ListOfEdges[i]) = ai;
				get<1>(ListOfEdges[i]) = bi;
				get<2>(ListOfEdges[i]) = w;
			}
			break;
		}
	}

	void addEdge(int from, int to, int weight)
	{
		if (!rState && from > to) swap(from, to);
		switch (vState)
		{
		case 'L':
			m++;
			if (rState) AdjList[from - 1][to] = weight;
			else
			{
				AdjList[from - 1][to] = weight;
				AdjList[to - 1][from] = weight;
			}
			break;
		case 'C':
			from--;
			to--;
			m++;
			if (rState) AdjMatrix[from][to] = weight;
			else
			{
				AdjMatrix[from][to] = weight;
				AdjMatrix[to][from] = weight;
			}
			break;
		case 'E':
			m++;
			ListOfEdges.resize(m);
			get<0>(ListOfEdges[m - 1]) = from;
			get<1>(ListOfEdges[m - 1]) = to;
			get<2>(ListOfEdges[m - 1]) = weight;
			break;
		}
	} // Check AdjList
	
	void removeEdge(int from, int to)
	{
		switch (vState)
		{
		case 'L':
		{
			auto result1 = AdjList[from - 1].find(to);
			auto result2 = AdjList[to - 1].find(from);
			if (result1 != AdjList[from - 1].end() && result2 != AdjList[to - 1].end())
			{
				AdjList[from - 1].erase(result1);
				AdjList[to - 1].erase(result2);
				m--;
			}
			else if (result2 != AdjList[to - 1].end())
			{
				AdjList[to - 1].erase(result2);
				m--;
			}
			else if (result1 != AdjList[from - 1].end())
			{
				AdjList[from - 1].erase(result1);
				m--;
			}
		}
			break;
		case 'C':
			from--;
			to--;
			if (AdjMatrix[from][to] != 0)
			{
				if (rState) AdjMatrix[from][to] = 0;
				else
				{
					AdjMatrix[from][to] = 0;
					AdjMatrix[to][from] = 0;
				}
				m--;
			}
			break;
		case 'E':
			for (auto it = ListOfEdges.begin(); it != ListOfEdges.end(); it++)
			{
				if (rState)
				{
					if (get<0>(*it) == from && get<1>(*it) == to)
					{
						ListOfEdges.erase(it);
						break;
					}
				}
				else
				{
					if (get<0>(*it) == from && get<1>(*it) == to || get<0>(*it) == to && get<1>(*it) == from)
					{
						ListOfEdges.erase(it);
						break;
					}
				}
			}
			m--;
			break;
		}
	}

	int changeEdge(int from, int to, int newWeight)
	{
		int x = 0;
		switch (vState)
		{
		case 'L':
		{
			auto result1 = AdjList[from - 1].find(to);
			auto result2 = AdjList[to - 1].find(from);
			if (result1 != AdjList[from - 1].end() && result2 != AdjList[to - 1].end())
			{
				x = result1->second;
				result1->second = newWeight;
				result2->second = newWeight;
			}
			else if (result1 != AdjList[from - 1].end())
			{
				x = result1->second;
				result1->second = newWeight;
			}
			if (result2 != AdjList[to - 1].end())
			{
				x = result2->second;
				result2->second = newWeight;
			}
			return x;
		}
			break;
		case 'C':
			from--;
			to--;
			x = AdjMatrix[from][to];
			AdjMatrix[from][to] = newWeight;
			if (!rState) AdjMatrix[to][from] = newWeight;
			break;
		case 'E':
			for (auto it = ListOfEdges.begin(); it != ListOfEdges.end(); it++)
			{
				if (rState)
				{
					if (get<0>(*it) == from && get<1>(*it) == to)
					{
						x = get<2>(*it);
						get<2>(*it) = newWeight;
						break;
					}
				}
				else
				{
					if (get<0>(*it) == from && get<1>(*it) == to || get<0>(*it) == to && get<1>(*it) == from)
					{
						x = get<2>(*it);
						get<2>(*it) = newWeight;
						break;
					}
				}
			}
			break;
		}
		return x;
	}

	void transformToAdjList()
	{
		switch (vState)
		{
		case 'L':
			//No need to transform
			break;
		case 'C':
			AdjList.resize(n);
			for (int i = 0; i < n; i++)
				for (int j = 0; j < n; j++)
				{
				if (AdjMatrix[i][j] != 0)
				{
					AdjList[i][j + 1] = AdjMatrix[i][j];
					if (!rState) AdjList[j][i + 1] = AdjMatrix[i][j];
				}
				}
			AdjMatrix.clear();
			AdjMatrix.shrink_to_fit();
			vState = 'L';
			break;
		case 'E':
			AdjList.resize(n);
			for (int i = 0; i < m; i++)
			{
				int ax = get<0>(ListOfEdges[i]);
				int ay = get<1>(ListOfEdges[i]);
				int aw = get<2>(ListOfEdges[i]);
				AdjList[ax - 1][ay] = aw;
				if (!rState) AdjList[ay - 1][ax] = aw;
			}
			ListOfEdges.clear();
			ListOfEdges.shrink_to_fit();
			vState = 'L';
			break;
		}
	}

	void transformToAdjMatrix()
	{
		switch (vState)
		{
		case 'L':
			AdjMatrix.resize(n);
			for (int i = 0; i < n; i++) AdjMatrix[i].resize(n);
			for (int i = 0; i < n; i++)
			{
				for (auto it = AdjList[i].begin(); it != AdjList[i].end(); it++)
				{
					int ay = it->first;
					int aw = it->second;
					AdjMatrix[i][ay - 1] = aw;
					if (!rState) AdjMatrix[ay - 1][i] = aw;
				}
			}
			AdjList.clear();
			AdjList.shrink_to_fit();
			vState = 'C';
			break;
		case 'C':
			//No need to transform
			break;
		case 'E':
			AdjMatrix.resize(n);
			for (int i = 0; i < n; i++) AdjMatrix[i].resize(n);
			for (int i = 0; i < m; i++)
			{
				int ax = get<0>(ListOfEdges[i]);
				int ay = get<1>(ListOfEdges[i]);
				int aw = get<2>(ListOfEdges[i]);
				AdjMatrix[ax - 1][ay - 1] = aw;
				if (!rState) AdjMatrix[ay - 1][ax - 1] = aw;
			}
			ListOfEdges.clear();
			ListOfEdges.shrink_to_fit();
			vState = 'C';
			break;
		}
	}

	void transformToListOfEdges()
	{
		switch (vState)
		{
		case 'L':
		{
			ListOfEdges.resize(m);
			int itk = 0;
			for (int i = 0; i < n; i++)
			{
				for (auto it = AdjList[i].begin(); it != AdjList[i].end(); it++)
				{
					int ay = it->first;
					int aw = it->second;
					if (!rState && i < ay || rState)
					{
						get<0>(ListOfEdges[itk]) = i + 1;
						get<1>(ListOfEdges[itk]) = ay;
						get<2>(ListOfEdges[itk]) = aw;
						itk++;
					}
				}
			}
			AdjList.clear();
			AdjList.shrink_to_fit();
			vState = 'E';
		}
			break;
		case 'C':
		{
			ListOfEdges.resize(m);
			int itk = 0;
			for (int i = 0; i < n; i++)
				for (int j = 0; j < n; j++)
				{
				if (AdjMatrix[i][j] != 0)
				{
					if (!rState && i < j || rState)
					{
						get<0>(ListOfEdges[itk]) = i + 1;
						get<1>(ListOfEdges[itk]) = j + 1;
						get<2>(ListOfEdges[itk]) = AdjMatrix[i][j];
						itk++;
					}
				}
				}
			AdjMatrix.clear();
			AdjMatrix.shrink_to_fit();
			vState = 'E';
		}
			break;
		case 'E':
			//No need to transform
			break;
		}
	}

	void writeGraph(string fileName)
	{
		const char * cName = fileName.c_str();
		freopen(cName, "w", stdout);
		switch (vState)
		{
		case 'L':
			cout << vState << ' ' << n << endl;
			cout << rState << ' ' << wState << endl;
			for (int i = 0; i < n; i++)
			{
				int adjListSize = AdjList[i].size();
				int k = 0;
				for (auto it = AdjList[i].begin(); it != AdjList[i].end(); it++)
				{
					if (k == adjListSize - 1)
					{
						if (wState) cout << it->first << ' ' << it->second;
						else cout << it->first;
					}
					else
					{
						if (wState) cout << it->first << ' ' << it->second << ' ';
						else cout << it->first << ' ';
					}
					k++;
				}
				cout << endl;
			}
			break;
		case 'C':
			cout << vState << ' ' << n << endl;
			cout << rState << ' ' << wState << endl;
			for (int i = 0; i < n; i++)
			{
				for (int j = 0; j < n - 1; j++)
					cout << AdjMatrix[i][j] << ' ';
				cout << AdjMatrix[i][n - 1] << endl;
			}
			break;
		case 'E':
			cout << vState << ' ' << n << ' ' << m << endl;
			cout << rState << ' ' << wState << endl;
			for (int i = 0; i < m; i++)
			{
				if (wState) cout << get<0>(ListOfEdges[i]) << ' ' << get<1>(ListOfEdges[i]) << ' ' << get<2>(ListOfEdges[i]);
				else cout << get<0>(ListOfEdges[i]) << ' ' << get<1>(ListOfEdges[i]);
				cout << endl;
			}
			break;
		}
	}

	Graph getSpanningTreeKruskal() 
	{
		Graph g;
		g.setStates('E', rState, wState, n, 0);
		transformToListOfEdges();
		sort(ListOfEdges.begin(), ListOfEdges.end(), cmpKruskal);
		DSU bom(n);
		for (int i = 0; i < m; i++)
		{
			int a = get<0>(ListOfEdges[i]);
			int b = get<1>(ListOfEdges[i]);
			int w = get<2>(ListOfEdges[i]);
			if (bom.find(a) != bom.find(b))
			{
				g.addEdge(a, b, w);
				bom.unite(a, b);
			}
		}
		return g;
	}

	Graph getSpanningTreeBoruvka()
	{
		Graph g;
		g.setStates('E', rState, wState, n, 0);
		transformToListOfEdges();
		DSU bom(n);
		int k = n;
		while (k > 1)
		{
			bool fl = true;
			vector <int> cheap(n + 1, -1);
			for (int i = 0; i < m; i++)
			{
				int a = bom.find(get<0>(ListOfEdges[i]));
				int b = bom.find(get<1>(ListOfEdges[i]));
				int w = get<2>(ListOfEdges[i]);
				if (a != b)
				{
					if (cheap[a] == -1 || w < get<2>(ListOfEdges[cheap[a]]))
					{
						cheap[a] = i;
						fl = false;
					}
					if (cheap[b] == -1 || w < get<2>(ListOfEdges[cheap[b]]))
					{
						cheap[b] = i;
						fl = false;
					}
				}
			}
			if (fl) break;
			for (int i = 1; i <= n; i++)
			{
				if (cheap[i] != -1)
				{
					int a = get<0>(ListOfEdges[cheap[i]]);
					int b = get<1>(ListOfEdges[cheap[i]]);
					int w = get<2>(ListOfEdges[cheap[i]]);
					if (bom.find(a) != bom.find(b))
					{
						g.addEdge(a, b, w);
						bom.unite(a, b);
						k--;
					}
				}
			}
		}
		return g;
	}

	Graph getSpanningTreePrima()
	{
		Graph g;
		g.setStates('E', rState, wState, n, 0);
		vector <int> vrtxMin(n + 1, 1000000);
		vector <int> vrtxPar(n + 1, -1);
		set <int> used;
		set <pair <int, int> > q;
		q.insert(make_pair(0, 1));
		vrtxMin[1] = 0;
		transformToAdjList();
		for (int i = 1; i <= n; i++) used.insert(i);
		for (int i = 0; i < n; i++)
		{
			if (q.empty()) q.insert(make_pair(0, *used.begin()));
			int v = q.begin()->second;
			q.erase(q.begin());
			for (auto j = AdjList[v - 1].begin(); j != AdjList[v - 1].end(); j++)
			{
				int to = j->first;
				int cost = j->second;
				if (cost < vrtxMin[to] && used.find(to) != used.end())
				{
					q.erase(make_pair(vrtxMin[to], to));
					vrtxMin[to] = cost;
					vrtxPar[to] = v;
					q.insert(make_pair(vrtxMin[to], to));
				}
			}
			used.erase(v);
			if (!q.empty()) g.addEdge(vrtxPar[q.begin()->second], q.begin()->second, q.begin()->first);
		}
		return g;
	}

	int checkEuler(bool &circleExist)
	{
		DSU bom(n);
		bool dsuFlag = false;
		int ans = 1, oddCount = 0;
		vector <int> power(n + 1, 0);
		transformToListOfEdges();
		for (int i = 0; i < m; i++)
		{
			power[get<0>(ListOfEdges[i])]++;
			power[get<1>(ListOfEdges[i])]++;
		}
		for (int i = 1; i <= n; i++)
		{
			if (power[i] % 2 == 1) 
			{
				oddCount++;
				ans = i;
			}
		}
		for (int i = 0; i < m; i++)
		{
			int a = get<0>(ListOfEdges[i]);
			int b = get<1>(ListOfEdges[i]);
			int w = get<2>(ListOfEdges[i]);
			if (bom.find(a) != bom.find(b))
			{
				bom.unite(a, b);
			}
		}
		dsuFlag = bom.checkConnectivity();
		(oddCount == 0) ? circleExist = true : circleExist = false;
		if ((oddCount == 0 || oddCount == 2) && dsuFlag) return ans;
		else return 0;
	}

	bool checkBridge(int n1, int n2)
	{
		Graph g;
		g.setStates('L', rState, wState, n, m);
		DSU bom(n);
		transformToAdjList();
		for (int i = 0; i < n; i++)
		{
			g.AdjList[i] = AdjList[i];
		}
		g.removeEdge(n1, n2);
		for (int i = 0; i < n; i++)
		{
			int a = i + 1;
			for (auto j = g.AdjList[i].begin(); j != g.AdjList[i].end(); j++)
			{
				int b = j->first;
				if (bom.find(a) != bom.find(b))
				{
					bom.unite(a, b);
				}
			}
		}
		return !bom.checkConnectivity();
	}

	bool checkBipart(vector <char> &marks)
	{
		queue <int> qVrtx;
		set <int> used;
		for (int i = 1; i <= n; i++) used.insert(i);
		qVrtx.push(1);
		marks[1] = 'A';
		int kMarks = 1;
		transformToAdjList();
		while (!qVrtx.empty())
		{
			int curVrtx = qVrtx.front() - 1;
			char workMark;
			qVrtx.pop();
			for (auto i = AdjList[curVrtx].begin(); i != AdjList[curVrtx].end(); i++)
			{
				if (marks[i->first] != 'A' && marks[i->first] != 'B')
				{
					marks[i->first] = marks[curVrtx + 1] == 'A' ? 'B' : 'A';
					qVrtx.push(i->first);
					kMarks++;
				}
				else if (marks[i->first] == marks[curVrtx + 1]) return false;
			}
			used.erase(curVrtx + 1);
			if (qVrtx.empty() && !used.empty()) qVrtx.push(*used.begin());
		}
		return true;
	}

	bool bipartDFS(int j, vector <bool> &used, vector <int> &curBipart, vector <bool> &visited)
	{
		if (used[j]) return false;
		used[j] = true;
		for (auto i = AdjList[j - 1].begin(); i != AdjList[j - 1].end(); i++)
		{
			int to = i->first;
			if ((curBipart[to] == -1) || bipartDFS(curBipart[to], used, curBipart, visited))
			{
				curBipart[to] = j;
				return true;
			}
		}
		return false;
	}

	int dinitzDFS(int source, int sink, int maxFlow, vector <int> &vrtxLayer, map < pair <int, int>, int > &edgesCap, map < pair <int, int>, int > &edgesFlow)
	{
		if (source == sink || maxFlow == 0) return maxFlow;
		for (auto it = AdjList[source - 1].begin(); it != AdjList[source - 1].end(); it++)
		{
			int to = it->first;
			if (vrtxLayer[to] != vrtxLayer[source] + 1) continue;
			int newFlow = min(maxFlow, edgesCap[make_pair(source, to)] - edgesFlow[make_pair(source, to)]);
			int pushed = dinitzDFS(to, sink, newFlow, vrtxLayer, edgesCap, edgesFlow);
			if (pushed)
			{
				auto it1 = AdjList[source - 1].find(to);
				it1->second -= pushed;
				edgesFlow[make_pair(source, to)] += pushed;
				auto it2 = AdjList[to - 1].find(source);
				it2->second += pushed;
				edgesFlow[make_pair(to, source)] -= pushed;
				return pushed;
			}
		}
		return 0;
	}

	vector <pair <int, int> > getMaximumMatchingBipart()
	{
		vector <int> curBipart(n + 1, -1);
		vector <pair <int, int> > ans;
		vector <char> marks(n + 1, 'N');
		vector <bool> visited(n + 1, false);
		vector <bool> used(n + 1, false);
		set <int> setVrtx;
		for (int i = 1; i <= n; i++) setVrtx.insert(i);
		transformToAdjList();
		for (int i = 1; i <= n; i++)
		{
			if (marks[i] == 'B') continue;
			for (auto j = AdjList[i - 1].begin(); j != AdjList[i - 1].end(); j++)
			{
				if (curBipart[j->first] == -1)
				{
					curBipart[j->first] = i;
					visited[i] = true;
					break;
				}
			}
		}
		if (checkBipart(marks))
		{
			for (int j = 1; j <= n; j++)
			{
				if (visited[j] || marks[j] == 'B')
				{
					used.assign(n + 1, false);
					bipartDFS(j, used, curBipart, visited);
				}
			}
		}
		for (int i = 1; i <= n; i++)
		{
			if (curBipart[i] != -1 && marks[i] == 'B' && setVrtx.find(curBipart[i]) != setVrtx.end())
			{
				ans.push_back(make_pair(i, curBipart[i]));
				setVrtx.erase(curBipart[i]);
			}
		}
		return ans;
	}

	vector <int> getEuleranTourFleri()
	{
		Graph g;
		g.setStates('E', rState, wState, n, m);
		vector <int> ans(m + 1);
		vector <int> power(n + 1, 0);
		transformToListOfEdges();
		for (int i = 0; i < m; i++)
		{
			power[get<0>(ListOfEdges[i])]++;
			power[get<1>(ListOfEdges[i])]++;
			get<0>(g.ListOfEdges[i]) = get<0>(ListOfEdges[i]);
			get<1>(g.ListOfEdges[i]) = get<1>(ListOfEdges[i]);
			get<2>(g.ListOfEdges[i]) = get<2>(ListOfEdges[i]);
		}
		bool circleFlag;
		int start = checkEuler(circleFlag);
		ans[0] = start;
		transformToAdjList();
		g.transformToAdjList();
		int curVrtx = start;
		for (int i = 0; i < m; i++)
		{
			bool stepFlag = false;
			for (auto j = g.AdjList[curVrtx - 1].begin(); j != g.AdjList[curVrtx - 1].end(); j++)
			{
				int neiVrtx = j->first;
				if (!g.checkBridge(curVrtx, neiVrtx) || power[curVrtx] == 1)
				{
					stepFlag = true;
					power[curVrtx]--;
					g.removeEdge(curVrtx, neiVrtx);
					curVrtx = neiVrtx;
					ans[i + 1] = curVrtx;
					power[neiVrtx]--;
					break;
				}
			}
			if (!stepFlag) for (auto j = g.AdjList[curVrtx - 1].begin(); j != g.AdjList[curVrtx - 1].end(); j++)
			{
				int neiVrtx = j->first;
				if (!g.checkBridge(curVrtx, neiVrtx) || power[neiVrtx] != 1)
				{
					power[curVrtx]--;
					g.removeEdge(curVrtx, neiVrtx);
					curVrtx = neiVrtx;
					ans[i + 1] = curVrtx;
					power[neiVrtx]--;
					break;
				}
			}
			stepFlag = false;
		}
		return ans;
	}

	vector <int> getEuleranTourEffective()
	{
		Graph g;
		g.setStates('E', rState, wState, n, m);
		stack <int> path;
		vector <int> ans(m + 1);
		vector <int> power(n + 1, 0);
		transformToListOfEdges();
		for (int i = 0; i < m; i++)
		{
			power[get<0>(ListOfEdges[i])]++;
			power[get<1>(ListOfEdges[i])]++;
			get<0>(g.ListOfEdges[i]) = get<0>(ListOfEdges[i]);
			get<1>(g.ListOfEdges[i]) = get<1>(ListOfEdges[i]);
			get<2>(g.ListOfEdges[i]) = get<2>(ListOfEdges[i]);
		}
		bool circleFlag;
		int start = checkEuler(circleFlag);
		ans[m] = start;
		int ansIt = 0;
		transformToAdjList();
		g.transformToAdjList();
		int curVrtx = start;
		for (int i = 0; i <= m; i++)
		{
			for (auto j = g.AdjList[curVrtx - 1].begin(); j != g.AdjList[curVrtx - 1].end(); j++)
			{
				int neiVrtx = j->first;
				path.push(neiVrtx);
				power[curVrtx]--;
				g.removeEdge(curVrtx, neiVrtx);
				curVrtx = neiVrtx;
				power[neiVrtx]--;
				break;
			}
			if (g.AdjList[curVrtx - 1].size() == 0 && !path.empty())
			{
				while (g.AdjList[path.top() - 1].size() == 0)
				{
					ans[ansIt] = path.top();
					path.pop();
					ansIt++;
					if (path.empty()) break;
					curVrtx = path.top();
				}
				//path.push(curVrtx);
			}
		}
		while (!path.empty())
		{
			ans[ansIt] = path.top();
			path.pop();
			ansIt++;
		}
		return ans;
	}

	Graph flowFordFulkerson(int sourse, int sink)
	{
		Graph g;
		g.setStates('L', rState, wState, n, m);
		transformToAdjList();
		for (int i = 0; i < n; i++)
		{
			g.AdjList[i] = AdjList[i];
			for (auto it = g.AdjList[i].begin(); it != g.AdjList[i].end(); it++)
			{
				it->second = 0;
			}
		}
		bool fl = true;
		while (fl)
		{
			queue <int> qVrtx;
			vector <bool> visited(n + 1, false);
			vector <int> marks(n + 1, 0);
			qVrtx.push(sourse);
			int curVrtx;
			int minAlpha = 1000000000;
			while (!qVrtx.empty())
			{
				curVrtx = qVrtx.front();
				visited[curVrtx] = true;
				qVrtx.pop();
				auto itG = AdjList[curVrtx - 1].begin();
				for (auto it = g.AdjList[curVrtx - 1].begin(); it != g.AdjList[curVrtx - 1].end(); it++)
				{
					int alpha = itG->second - it->second;
					itG++;
					if (!visited[it->first] && alpha > 0)
					{
						marks[it->first] = curVrtx;
						qVrtx.push(it->first);
						visited[it->first] = true;
						if (alpha < minAlpha) minAlpha = alpha;
					}
				}
				for (int i = 0; i < n; i++)
				{
					if (!visited[i + 1] && !visited[curVrtx] && AdjList[i].find(curVrtx) != AdjList[i].end())
					{
						int bom = AdjList[i].at(curVrtx);
						int alpha = AdjList[i].at(curVrtx);
						marks[curVrtx] = -i - 1;
						qVrtx.push(i + 1);
						visited[i + 1] = true;
						if (alpha < minAlpha) minAlpha = alpha;
					}
				}
			}
			if (curVrtx != sink)
			{
				fl = false;
				continue;
			}
			while (curVrtx != 1)
			{
				if (marks[curVrtx] > 0) g.AdjList[marks[curVrtx] - 1].at(curVrtx) += minAlpha;
				else g.AdjList[abs(marks[curVrtx]) - 1].at(curVrtx) -= minAlpha;
				curVrtx = abs(marks[curVrtx]);
			}
		}
		return g;
	}

	Graph flowDinitz(int sourse, int sink)
	{
		Graph g;
		Graph ans;
		map < pair <int, int>, int > edgesFlow;
		map < pair <int, int>, int > edgesCap;
		g.setStates('L', rState, wState, n, 2 * m);
		ans.setStates('L', rState, wState, n, 0);
		transformToAdjList();
		for (int i = 0; i < n; i++)
		{
			for (auto it = AdjList[i].begin(); it != AdjList[i].end(); it++)
			{
				int to = it->first;
				g.AdjList[to - 1][i + 1] = 0;
				g.AdjList[i][to] = it->second;
				edgesCap[make_pair(i + 1, to)] = it->second;
				edgesCap[make_pair(to, i + 1)] = it->second;
				edgesFlow[make_pair(i + 1, to)] = 0;
				edgesFlow[make_pair(to, i + 1)] = it->second;
			}
		}
		bool fl = true;
		while (fl)
		{
			vector <int> vrtxLayer(n + 1, -1);
			queue <int> qVrtx;
			qVrtx.push(sourse);
			vrtxLayer[sourse] = 0;
			while (!qVrtx.empty() && vrtxLayer[sink] == -1)
			{
				int curVrtx = qVrtx.front();
				qVrtx.pop();
				for (auto it = g.AdjList[curVrtx - 1].begin(); it != g.AdjList[curVrtx - 1].end(); it++)
				{
					int to = it->first;
					if (vrtxLayer[to] == -1 && edgesFlow[make_pair(curVrtx, to)] < edgesCap[make_pair(curVrtx, to)])
					{
						qVrtx.push(to);
						vrtxLayer[to] = vrtxLayer[curVrtx] + 1;
					}
				}
			}
			int bom = g.dinitzDFS(sourse, sink, 1000000000, vrtxLayer, edgesCap, edgesFlow);
			fl = bom;
			if (!fl) break;
		}
		for (int i = 0; i < n; i++)
		{
			for (auto it = g.AdjList[i].begin(); it != g.AdjList[i].end(); it++)
			{
				int to = it->first;
				if ((i >= to))
					ans.addEdge(to, i + 1, it->second);
			}
		}
		return ans;
	}
};

